# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
PATH="$HOME/.local/bin:$HOME/bin:$HOME/.config/composer/vendor/bin:$PATH"
export PATH
PS1="\[\[\033[1;31m\]\u@\h:\w\\[\033[00m\]\$ "
# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

alias nv='nvim'
